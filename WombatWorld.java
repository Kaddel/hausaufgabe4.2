import greenfoot.*;

/**
 * Write a description of class Wombat here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class WombatWorld extends World
{
    /**
     * Create a new world with 10x15 cells and
     * with a cell size of 60x60 pixels
     */
    public WombatWorld() 
    {
        super(14,3, 60);        
        setBackground("cell.jpg");

        prepare();
    }

    /**
     * Prepare the world for the start of the program. That is: create the initial
     * objects and add them to the world.
     */
    private void prepare()
    {
        Wombat wombat = new Wombat();
        addObject(wombat, 3, 3);

        wombat.setLocation(0,1);
        Point point = new Point();
        addObject(point,2,1);
        Point point2 = new Point();
        addObject(point2,2,1);
        Point point3 = new Point();
        addObject(point3,4,1);
        Enemy enemy = new Enemy();
        addObject(enemy,3,0);
        removeObject(enemy);
        point3.setLocation(2,1);
        Enemy enemy2 = new Enemy();
        addObject(enemy2,3,0);
        Enemy enemy3 = new Enemy();
        addObject(enemy3,3,2);
        Enemy enemy4 = new Enemy();
        addObject(enemy4,4,2);
        point3.setLocation(2,1);
        Point point4 = new Point();
        addObject(point4,2,1);
        Point point5 = new Point();
        addObject(point5,5,1);
        Point point6 = new Point();
        addObject(point6,5,1);
        Point point7 = new Point();
        addObject(point7,6,1);
        Point point8 = new Point();
        addObject(point8,6,1);
        Point point9 = new Point();
        addObject(point9,6,1);
        removeObject(enemy2);
        point4.setLocation(2,2);
        point3.setLocation(2,0);
        point2.setLocation(2,1);
        point3.setLocation(2,1);
        point4.setLocation(2,1);
        point6.setLocation(5,1);
        point9.setLocation(6,2);
        point8.setLocation(6,1);
        point9.setLocation(6,1);
        Enemy enemy5 = new Enemy();
        addObject(enemy5,7,0);
        Enemy enemy6 = new Enemy();
        addObject(enemy6,7,2);
        Point point10 = new Point();
        addObject(point10,8,1);
        Point point11 = new Point();
        addObject(point11,8,1);
        removeObject(enemy3);
        removeObject(enemy4);
        Enemy enemy7 = new Enemy();
        addObject(enemy7,4,2);
        Enemy enemy8 = new Enemy();
        addObject(enemy8,3,2);
        removeObject(point4);
        removeObject(point9);
        point3.setLocation(2,2);
        removeObject(point3);
        point6.setLocation(5,1);
        point11.setLocation(10,1);
        point10.setLocation(9,1);
        point11.setLocation(9,1);
        enemy5.setLocation(8,0);
        enemy6.setLocation(8,2);
        enemy7.setLocation(3,0);
        point8.setLocation(4,1);
        point7.setLocation(4,1);
        point6.setLocation(6,1);
        point5.setLocation(6,1);
        Point point12 = new Point();
        addObject(point12,7,1);
        point12.setLocation(11,1);
        enemy5.setLocation(7,0);
        enemy6.setLocation(7,2);
        point11.setLocation(8,1);
        point10.setLocation(8,1);
        point11.setLocation(8,1);
        Point point13 = new Point();
        addObject(point13,8,1);
        point2.setLocation(1,1);
        point.setLocation(1,1);
        enemy7.setLocation(2,0);
        enemy8.setLocation(2,2);
        point8.setLocation(3,1);
        point7.setLocation(3,1);
        point6.setLocation(5,1);
        point5.setLocation(5,1);
        enemy5.setLocation(6,0);
        enemy6.setLocation(6,2);
        point13.setLocation(7,1);
        point11.setLocation(7,1);
        point10.setLocation(7,1);
        point12.setLocation(9,1);
        point2.setLocation(1,1);
        point8.setLocation(3,1);
        point6.setLocation(5,1);
        point13.setLocation(7,2);
        point11.setLocation(7,1);
        point13.setLocation(7,1);
        point12.setLocation(9,1);
        removeObject(enemy7);
        removeObject(enemy5);
        point2.setLocation(1,1);
        point8.setLocation(3,1);
        point6.setLocation(5,1);
        point13.setLocation(7,2);
        point11.setLocation(7,1);
        point13.setLocation(7,1);
        Enemy enemy9 = new Enemy();
        addObject(enemy9,4,2);
        enemy8.setLocation(0,0);
        point2.setLocation(1,1);
        enemy9.setLocation(2,0);
        enemy6.setLocation(4,0);
        Enemy enemy10 = new Enemy();
        addObject(enemy10,6,2);
        Enemy enemy11 = new Enemy();
        addObject(enemy11,8,2);
        Point point14 = new Point();
        addObject(point14,9,1);
        Point point15 = new Point();
        addObject(point15,9,1);
        point2.setLocation(0,1);
        point.setLocation(0,1);
        enemy8.setLocation(1,0);
        point8.setLocation(2,1);
        point7.setLocation(2,1);
        enemy9.setLocation(3,0);
        enemy6.setLocation(5,0);
        point6.setLocation(4,1);
        point5.setLocation(4,1);
        point13.setLocation(6,1);
        point11.setLocation(6,1);
        point10.setLocation(6,1);
        enemy10.setLocation(7,2);
        enemy11.setLocation(9,2);
        point15.setLocation(8,1);
        point14.setLocation(8,1);
        point12.setLocation(8,1);
        point2.setLocation(0,1);
        point2.setLocation(0,0);
        removeObject(point2);
        point.setLocation(0,0);
        removeObject(point);
        enemy8.setLocation(1,2);
        enemy9.setLocation(3,2);
        enemy6.setLocation(5,2);
        removeObject(enemy8);
        point8.setLocation(1,1);
        point7.setLocation(1,1);
        enemy9.setLocation(2,2);
        point6.setLocation(3,1);
        point5.setLocation(3,1);
        enemy6.setLocation(4,2);
        point13.setLocation(5,1);
        point11.setLocation(5,1);
        point10.setLocation(5,1);
        enemy10.setLocation(6,2);
        point15.setLocation(7,1);
        point14.setLocation(7,1);
        point12.setLocation(7,1);
        removeObject(enemy11);
        enemy10.setLocation(5,2);
        point13.setLocation(6,1);
        point11.setLocation(6,1);
        point10.setLocation(6,1);
        Point point16 = new Point();
        addObject(point16,9,1);
        Point point17 = new Point();
        addObject(point17,9,1);
        Enemy enemy12 = new Enemy();
        addObject(enemy12,4,0);
        removeObject(enemy6);
        enemy10.setLocation(5,0);
        enemy9.setLocation(2,0);
        point17.setLocation(9,1);
        removeObject(point13);
        removeObject(point11);
        removeObject(point15);
        removeObject(point14);
        Point point18 = new Point();
        addObject(point18,11,1);
        Point point19 = new Point();
        addObject(point19,13,1);
        point17.setLocation(9,1);
        Enemy enemy13 = new Enemy();
        addObject(enemy13,8,0);
        Enemy enemy14 = new Enemy();
        addObject(enemy14,10,0);
        Enemy enemy15 = new Enemy();
        addObject(enemy15,12,0);
        removeObject(enemy12);
        removeObject(enemy10);
        removeObject(enemy13);
        removeObject(enemy14);
        removeObject(enemy15);
        removeObject(point19);
        removeObject(point18);
        removeObject(point17);
        removeObject(point16);
        removeObject(point12);
        removeObject(point10);
        removeObject(point6);
        removeObject(point5);
        removeObject(point8);
        removeObject(point7);
        enemy9.setLocation(3,0);
        Point point20 = new Point();
        addObject(point20,2,1);
        Point point21 = new Point();
        addObject(point21,2,1);
        Point point22 = new Point();
        addObject(point22,4,1);
        Point point23 = new Point();
        addObject(point23,4,1);
        Point point24 = new Point();
        addObject(point24,4,1);
        Point point25 = new Point();
        addObject(point25,4,1);
        Point point26 = new Point();
        addObject(point26,4,1);
        Point point27 = new Point();
        addObject(point27,5,1);
        Point point28 = new Point();
        addObject(point28,5,1);
        Point point29 = new Point();
        addObject(point29,5,1);
        Point point30 = new Point();
        addObject(point30,5,1);
        Enemy enemy16 = new Enemy();
        addObject(enemy16,1,2);
        removeObject(point26);
        removeObject(point25);
        removeObject(point24);
        removeObject(point23);
        removeObject(point22);
        removeObject(point30);
        removeObject(point29);
        removeObject(point28);
        removeObject(point27);
        Point point31 = new Point();
        addObject(point31,5,1);
        Point point32 = new Point();
        addObject(point32,5,1);
        Point point33 = new Point();
        addObject(point33,5,1);
        Point point34 = new Point();
        addObject(point34,6,1);
        Point point35 = new Point();
        addObject(point35,6,1);
        Point point36 = new Point();
        addObject(point36,7,1);
        Enemy enemy17 = new Enemy();
        addObject(enemy17,1,0);
        point33.setLocation(2,1);
        point32.setLocation(2,1);
        point35.setLocation(5,1);
        point36.setLocation(6,1);
        Point point37 = new Point();
        addObject(point37,6,1);
        Point point38 = new Point();
        addObject(point38,6,1);
        Point point39 = new Point();
        addObject(point39,6,1);
        point35.setLocation(5,2);
        point31.setLocation(5,1);
        point35.setLocation(5,1);
        point35.setLocation(4,1);
        point31.setLocation(4,1);
        point39.setLocation(5,1);
        point38.setLocation(5,1);
        point37.setLocation(5,1);
        point36.setLocation(5,1);
        removeObject(point34);
    }
}
