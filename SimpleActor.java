import greenfoot.*;

/**
 * Write a description of class SimpleActor here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class SimpleActor extends Actor
{
    private static final int EAST = 0;
    private static final int WEST = 1;
    private static final int NORTH = 2;
    private static final int SOUTH = 3;

    private int direction;
    private int points;
    private boolean error;

    /**
     * SimpleActor Constructor
     *
     */
    public SimpleActor()
    {
        super();
        setDirection(EAST);
        points = 0;
        error = false;
    }

     /**
     * 
     *Skaliert ein Objekt, bevor es zu der Welt hinzugefuegt wird.
     * @param World world
     */
    
    protected void addedToWorld(World world)
    {
        super.addedToWorld(world);
        GreenfootImage img = this.getImage();
        if(! ((img.getWidth() < world.getCellSize()) && img.getHeight() < world.getCellSize())){
            if(img.getWidth() > img.getHeight()){
                img.scale(world.getCellSize() , (int) (img.getHeight() * world.getCellSize()/ img.getWidth()));
            } else {
                img.scale((int) (img.getWidth()  * world.getCellSize()/ img.getHeight()) , world.getCellSize());
            }
            // img.scale(this.getWorld().getCellSize(),this.getWorld().getCellSize());
            this.setImage(img);
        }
    }

    
    
    /**
     * Private Methode, die nur innerhalb dieser Klasse aufgerufen werden kann. 
     * Setzt die Richtung von dem Objekt.
     *
     * @param direction A parameter
     */
    private void setDirection(int direction)
    {
        this.direction = direction;
        switch(direction) {
            case SOUTH :
                setRotation(90);
                break;
            case EAST :
                setRotation(0);
                break;
            case NORTH :
                setRotation(270);
                break;
            case WEST :
                setRotation(180);
                
                break;
            default :
            break;
        }
    }

    /**
     * Das Objekt wird um ein Feld nach vorne gesetzt in Abhaengigkeit von der Richtung.
     *
     */
    public void vor()
    {
        if(!error){
            if(!vornFrei()){
                System.err.println("Fehler");
                error=true;
                //Greenfoot.playSound("Laser.wav");
            }
            else{
                switch(direction) {
                    case SOUTH :
                    setLocation(getX(), getY() + 1);
                    break;
                    case EAST :
                    setLocation(getX() + 1, getY());
                    break;
                    case NORTH :
                    setLocation(getX(), getY() - 1);
                    break;
                    case WEST :
                    setLocation(getX() - 1, getY());
                    break;
                }
            }
            Greenfoot.delay(1);
        }
    }

    /**
     * Ueberprueft, ob vor dem Objekt frei ist.
     *
     *
     * @return Gibt true zurueck, wenn vorne Frei ist, ansonsten false
     */
    public boolean vornFrei(){
        World myWorld = getWorld();
        int x = getX();
        int y = getY();
        switch(direction) {
            case SOUTH :
            y++;
            break;
            case EAST :
            x++;
            break;
            case NORTH :
            y--;
            break;
            case WEST :
            x--;
            break;
        }
        if (x >= myWorld.getWidth() || y >= myWorld.getHeight()) {
            return false;
        }
        else if (x < 0 || y < 0) {
            return false;
        }
        else if (myWorld.getObjectsAt(x,y,Enemy.class).size()>0){
            return false;
        }
        return true;

    }

    /**
     * Nimmt, bzw. Attackiert ein anderes Objekt.
     *
     */
    public void nimm()
    {
        if(!error){
            Actor point = getOneObjectAtOffset(0,0,Point.class);
            if(point!= null){
                points ++;
                getWorld().removeObject(point);
                //Greenfoot.playSound("Dogfight.wav");
            }
            else{
                System.err.println("Kein Object da");
                error=true;
            }
            Greenfoot.delay(1);
        }
    }
    
    
    
    /**
     * Ueberprueft ob ein Punkt/Gegner da ist.
     *
     * @return Gibt true zurueck, wenn ein Punkt/Gegner da ist, ansonsten false.
     */
    public boolean punktDa()
    {
        Actor point = getOneObjectAtOffset(0,0,Point.class);
        if(point!= null){
            return true;
        }
        else{
            return false;
        }
    }

     /**
     * Ueberprueft ob Punkte gesammelt wurden.
     *
     * @return Gibt true zurueck, wenn keine Punkte/Gegner gesammelt wurden, ansonsten false.
     */
    
    public boolean punkteGleichNull()
    {
        if(points == 0){
            return true;
        } else {
           return false; 
        }
    }
    
    
    /**
     * Legt ein Objekt in die Welt ab. 
     * 
     */
    public void gib()
    {
        //Zeile die Angepasst werden muss bei eigenen Objekten!
        Point point = new Point();
        if(!error){
            if (points == 0){
                System.err.println("Kein Object da");
                error = true;
            }
            else{
                World myWorld = getWorld();
                myWorld.addObject(point, getX(), getY()); 
                points--;
            }
            Greenfoot.delay(1);
        }   
    }

    /**
     * Dreht das Objekt gegen den Uhrzeigersinn.
     *
     */
    public void linksUm(){
        if(!error){
            GreenfootImage img = this.getImage();
            switch(direction) {
                case SOUTH :
                    setDirection(EAST);
                    break;
                case EAST :
                    setDirection(NORTH);
                    break;
                case NORTH :
                    img.mirrorVertically();
                    this.setImage(img);
                    setDirection(WEST);
                    break;
                case WEST :
                    img.mirrorVertically();
                    this.setImage(img);
                    setDirection(SOUTH);
                    break;
            }
            Greenfoot.delay(1);
        }
    }

}
