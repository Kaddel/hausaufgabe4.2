import greenfoot.*;

/**
 * Write a description of class Point here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Point extends Actor
{
 
     /**
     * 
     *Skaliert ein Objekt, bevor es zu der Welt hinzugefuegt wird.
     * @param World world
     */
    
    protected void addedToWorld(World world)
    {
        super.addedToWorld(world);
        GreenfootImage img = this.getImage();
        if(! ((img.getWidth() < world.getCellSize()) && img.getHeight() < world.getCellSize())){
            if(img.getWidth() > img.getHeight()){
                img.scale(world.getCellSize() , (int) (img.getHeight() * world.getCellSize()/ img.getWidth()));
            } else {
                img.scale((int) (img.getWidth()  * world.getCellSize()/ img.getHeight()) , world.getCellSize());
            }
            // img.scale(this.getWorld().getCellSize(),this.getWorld().getCellSize());
            this.setImage(img);
        }
        java.util.List l = getIntersectingObjects(Point.class);
        setRotation(l.size()*10);        
    }
    
}
