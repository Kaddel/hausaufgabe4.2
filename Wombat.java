import greenfoot.*;

/**
 * Write a description of class Wombagt here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Wombat extends SimpleActor
{
    /**
     * Act - do whatever the Wombagt wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {   
        punkteAblegen();
    }    
    
    public int getOperator(){
        
        int oben = 0;
     int unten = 0;
     //addieren: nichts oben oder unten
     //subtrahieren: einer oben, keiner unten
     //multiplizieren: keiner drüber, einer drunter
     //dividieren: ein Feind drüber, einer drunter
     linksUm();
     if(vornFrei()){
         oben = 0;}
     else{
         oben = 1;
     }
     
     linksUm();
     linksUm();
     if(vornFrei()){
        unten = 0;
        }
     else{
        unten = 1;
        }
     linksUm();
     if(oben == 0 && unten == 0){System.out.println("add"); return 0;} //Addition
     if(oben == 1 && unten == 0) {System.out.println("sub"); return 1;} //Subtraktion
     if(oben == 0 && unten == 1){System.out.println("mult"); return 2;} //Multiplikation
     if(oben == 1 && unten == 1){System.out.println("div"); return 3;} //Division
     
     return 0;
        
    }
    
    public int berechne(){
    
     int operator = getOperator(); //Funktion, die den Operator bestimmt
        int zahl1 = 0;
        vor();
        if(punktDa()){
                zahl1 = zaehlePunkte(); // Funktion, die die Punkte auf dem Feld zaehlt.
        } else if(vornFrei()) {
                zahl1 = berechne(); // Rekursiver Aufruf, das ist dann der Fall, wenn in dem Syntaxdiagramm wieder ein Ausdruck gewählt wurde.
        }
        if(vornFrei()) vor();
        int zahl2 = 0;
        if(punktDa()){
                zahl2 = zaehlePunkte();
        } else if(vornFrei()) {
                zahl2 = berechne();
        }
        if(operator == 0) {System.out.println("additi" + zahl1 + zahl2); return zahl1 + zahl2;}
        if(operator == 1) {System.out.println("subtra" + zahl1  + zahl2); return zahl1 - zahl2;}
        if(operator == 2) {System.out.println("multi" + zahl1 + zahl2); return zahl1 * zahl2;}
        if(operator == 3) {System.out.println("divisi" +zahl1 + zahl2); return zahl1 / zahl2;}
        return 0;
    
    }
    
    public int zaehlePunkte(){
    int punkt = 0;
    while(punktDa()){nimm(); punkt++;};
    return punkt;
    }
    
    public void punkteAblegen(){
        int punkte = berechne();
        System.out.println(punkte);
        if(punkte < 0){
        linksUm();
        linksUm();
        vor();
        gib();
        linksUm();
        linksUm();
        vor();
        while(punkte < 0){
        gib();
     punkte++;
        System.out.println(punkte + "while <=0");}
         }
        while(punkte > 0){
        gib();
        punkte--;
        }
    
    }
    
}
